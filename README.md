# Summary
Ansible role for MX Linux/Debian, installing/updating container tools in the podman ecosystem:
- `podman`
- `podman-compose`

By default, installs or updates `podman` and `podman-compose` with support for:
- rootless mode userspace networking (`slirp4netns`)
- container-to-container name resolution (`netavark` and `aardvark-dns`)

# Usage
See and override `defaults/main.yml`

## Rootless networking + DNS resolution
Let containers resolve each other's names from a `docker-compose.yml` file.

In the playbook:
```yaml
install_podman:                 true
install_podman_compose:         true
podman_enable_rootless:         true
podman_enable_dns_resolution:   true
```

# Notes
## podman vs. docker comparison
Both build and run OCI-format containers. It's possible to `alias docker=podman` and use it identically (or nearly, with some nice benefits).

### podman security benefits
- Doesn't run a root-privileged daemon
- Rootless mode: run containers as root as an unprivileged container-host user, through `setuid` and `setgid` mappings
- No root-equivalent `docker` group in your default container development user
- Runs processes, exits when container finishes

## One-time podman system reset for using netavark/aardvark-dns
**WARNING**: deletes all podman containers + images + networks

After applying the playbook, you need to reset podman so it uses netavark and aardvark-dns. Run this manually once, as the user invoking podman: `podman system reset --force`

References:
https://github.com/containers/aardvark-dns/
https://github.com/containers/netavark/
https://github.com/containers/podman/blob/main/docs/tutorials/basic_networking.md

# Supported platforms
Probably, similar Ubuntu/Debian-based environments with some tweaking

Tested on MX Linux 23 (which uses Debian stable as package base)